velcro
    + strong
    + cheap
    - loud
    - industrial (10lb) too strong? or just right?
    + gravity self-realigning
magnet
    + strong, if big
    + self-realigning
    - shatter-risk (need to proof w/ a protective plastic layer or epoxy
    - not cheap?
    + cheap, but then aliexpress delivery times
3dp clip
    + cheap
    + adjustable strong
    - design work
    - not self-realigning
    --- actually, could actively prevent realign

if buy:

velcro
    1 roll

magnet
    1 pc sys
        1 per panel, 1 per leg = 2 per side
        4 * 2 = 8 per chair
    2 piece system
        2 per panel, 2 per leg = 4 per side
        4 * 4 = 16 per chair
        
